import { resolve } from "path";
import conexion  from "./conexion.js";
import { rejects } from "assert";

// generar un objeto para las operaciones

var  AlumnosDb={

};

AlumnosDb.insertar = function insertar(alumno){

    return new Promise((resolve,rejects)=>{
 let sqlConsulta = "Insert into alumnos set ? ";
 conexion.query(sqlConsulta,alumno, function(err,res){
    if(err){

        console.log("Surgio un error ", err.message);
        rejects(err);
    }
    else {

       const alumno = {
        id:res.id,
    
             }

        resolve(alumno);
    }
     

       });


    });
}

AlumnosDb.mostrarTodos = function mostrarTodos() {
    return new Promise((resolve, reject)=> {
        var sqlConsulta = "select * from alumnos";
        conexion.query(sqlConsulta, null, function(err, res) {
            if(err) { 
                console.log("Surgio un error");
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}


export default AlumnosDb;
