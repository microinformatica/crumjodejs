import http from 'http';
import path from 'path';
const puerto =3000;
import express from 'express';
import json from 'body-parser'
import { fileURLToPath } from 'url';
import misRutas  from './router/index.js';
//import misDatos from  './models/datos.js'




const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();



// Asignaciones 
app.set("view engine","ejs");

app.use(json.urlencoded({extended:true}));

// asignar al objeto informacion
app.use(express.static(__dirname + '/public'));

//app.use(misDatos.alumnos);
//console.log(alumnos);

app.use(misRutas.router);

app.listen(puerto,()=>{

    console.log("escuchando servidor");
})


